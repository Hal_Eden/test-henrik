<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table = 'country';

    public function zones()
    {
        return $this->hasMany('App\Zone', 'country_code', 'country_code');
    }

}
