<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gmt extends Model
{
    protected $fillable = ['timezone'];
}
