<?php

namespace App\Http\Controllers;

use App\Capitals;
use App\Country;
use App\Gmt;
use App\ListTimezone;
use App\ListZone;
use App\Timezone;
use App\Utc;
use App\Zone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PragmaRX\Countries\Package\Countries;
use PragmaRX\Countries\Package\Services\Config;

class MainController extends Controller
{

//    Date and Time converting form Page
    public function dateTime(Request $request) {

//        Get Timezones Lists
        $timezones = ListTimezone::with('zones')->get();
        $utcs = Utc::all();
        $gmts = Gmt::all();
        $countries = new Countries(new Config([
            'hydrate' => [
                'elements' => [
                    'currencies' => true,
                    'flag' => true,
                    'timezones' => true,
                    'cities' => true
                ],
            ],
        ]));

//        Filter Time Zones List To Get Locations With Time Zones
        $locations = array();

//        Get Locations From Timezones
        foreach ($timezones as $timezone) {

            $location_zone = [];

////            Location Zone And DST Status
//            $location_zone['offset'] = $timezone->offset;
//            $location_zone['isdst'] = $timezone->isdst;
//            $location_zone['type'] = 'zone';
//
////            Get Abbr List
//            $location_zone['value'] = $timezone->abbr;
//            $location_zone['abbr'] = "zone";
//            $locations[] = $location_zone;

//            Get Timezones List
//            $location_zone['value'] = preg_replace('/\((.+)\) /i', "", $timezone->text);
//            $locations[] = $location_zone;
//            if(!preg_match('/(utc|gmt)/i', $timezone->value)) {
//                $location_zone['value'] = $timezone->value;
//                $locations[] = $location_zone;
//            }

        }

//        Get Locations From UTC
        foreach ($utcs as $utc) {

            $location_zone = [];

//            Location Zone / DST Status / Type
            $offset = explode(' ', $utc->timezone);
            $location_zone['offset'] = count($offset) > 1 ? $offset[1] : "0";
            $location_zone['isdst'] = 0;
            $location_zone['type'] = 'zone';
            $location_zone['value'] = $utc->timezone;
            $location_zone['abbr'] = "zone";

//            Push To The Main List
            $locations[] = $location_zone;

        }

//        Get Locations From GMT
        foreach ($gmts as $gmt) {

            $location_zone = [];

//            Location Zone / DST Status / Type
            $offset = explode(' ', $gmt->timezone);
            $location_zone['offset'] = count($offset) > 1 ? $offset[1] : "0";
            $location_zone['isdst'] = 0;
            $location_zone['type'] = 'zone';
            $location_zone['value'] = $gmt->timezone;
            $location_zone['abbr'] = "zone";

//            Push To The Main List
            $locations[] = $location_zone;

        }

        $countries = $countries->all();

        foreach($countries as $country) {

            $country_name = $country->name["common"];
            $cca2 = is_file(public_path('/img/flags/' . strtolower($country["cca2"]) . '.png')) ? strtolower($country["cca2"]) : 'ufi';

            foreach($country->cities as $city) {

                if(is_array($city) && isset($city["timezone"]) && $city["timezone"]) {

                    $location_zone = [];

//                      Location Zone And DST Status
                    $location_zone['offset'] = $city["timezone"];
                    $location_zone['isdst'] = 0;
                    $location_zone['type'] = 'loc';
                    $location_zone['value'] = $country_name . ', ' . $city["nameascii"];
                    $location_zone['abbr'] = $cca2 ?: "";

//                      Push To The Main List
                    $locations[] = $location_zone;

                }

            }

        }

//        dd(public_path('/img/flags/' . $country["cca2"] . '.png'));

//        Add Additional Countries
        $russia = array(
            "offset" => "Europe/Moscow",
            "isdst" => 0,
            "type" => "loc",
            "value" => "Russia, Moscow",
            "abbr" => "RU"
        );

        $locations[] = $russia;

//        Get Form Settings Data
        $form_params = array();
        $form_params["t"] = $request->t;
        $form_params["tf"] = $request->tf;

        $form_params["lz"] = $request->lz;
        $form_params["lt"] = $request->lt;

        $form_params["cz"] = $request->cz;
        $form_params["ct"] = $request->ct;

        $form_params["ae"] = $request->ae;
        $form_params["aet"] = $request->aet;

        $form_params["events"] = $request->events;

//        dd(base_path());

        return view('pages.index', compact('locations', 'form_params'));

    }

    public function searchData() {

//        Get Timezones Lists
        $timezones = ListTimezone::with('zones')->get();
        $utcs = Utc::all();
        $gmts = Gmt::all();
        $countries = new Countries(new Config([
            'hydrate' => [
                'elements' => [
                    'currencies' => true,
                    'flag' => true,
                    'timezones' => true,
                    'cities' => true
                ],
            ],
        ]));

//        Filter Time Zones List To Get Locations With Time Zones
        $locations = array();

//        Get Locations From Timezones
        foreach ($timezones as $timezone) {

            $location_zone = [];

//            Location Zone And DST Status
            $location_zone['offset'] = $timezone->offset;
            $location_zone['isdst'] = $timezone->isdst;
            $location_zone['type'] = 'zone';

//            Get Abbr List
            if(!preg_match('/(utc|gmt)/i', $timezone->abbr)) {
                $locations[$timezone->abbr] = $location_zone;
            }
            $locations[preg_replace('/\((.+)\) /i', "", $timezone->text)] = $location_zone;
            if(!preg_match('/(utc|gmt)/i', $timezone->value)) {
                $locations[$timezone->value] = $location_zone;
            }

        }

//        Get Locations From UTC
        foreach ($utcs as $utc) {

            $location_zone = [];

//            Location Zone / DST Status / Type
            $offset = explode(' ', $utc->timezone);
            $location_zone['offset'] = count($offset) > 1 ? $offset[1] : "0";
            $location_zone['isdst'] = 0;
            $location_zone['type'] = 'zone';

//            Get Abbr List
            $locations[$utc->timezone] = $location_zone;

        }

//        Get Locations From GMT
        foreach ($gmts as $gmt) {

            $location_zone = [];

//            Location Zone / DST Status / Type
            $offset = explode(' ', $gmt->timezone);
            $location_zone['offset'] = count($offset) > 1 ? $offset[1] : "0";
            $location_zone['isdst'] = 0;
            $location_zone['type'] = 'zone';

//            Get Abbr List
            $locations[$gmt->timezone] = $location_zone;

        }

//        Get Countries And Cities
//        foreach ($cities as $city) {
//
//            $location_zone = [];
//
////            Location Zone / DST Status / Type
//            $location_zone['offset'] = round($city->offset / 3600, 1);
//            $location_zone['isdst'] = $city->isdst;
//            $location_zone['type'] = 'country';
//
////            Get Abbr List
//            $locations[$city->country] = $location_zone;
//
//        }
        $countries = $countries->all();

        foreach($countries as $country) {

            $country_name = $country->name["common"];

            foreach($country->cities as $city) {

                if(is_array($city) && isset($city["timezone"])) {

                    $location_zone = [];

//                    Location Zone And DST Status
                    $location_zone['offset'] = $city["timezone"];
                    $location_zone['isdst'] = 0;
                    $location_zone['type'] = 'loc';

//                    Get Abbr List
                    $locations[$country_name . ', ' . $city["name"]] = $location_zone;

                }

            }

        }

        return response()->json([
            'hints' => $locations
        ]);

    }

}
