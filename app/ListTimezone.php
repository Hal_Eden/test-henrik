<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListTimezone extends Model
{
    protected $fillable = ['value', 'abbr', 'offset', 'isdst', 'text'];

    public function zones() {

        return $this->hasMany('App\ListZone', 'timezone_id', 'id');

    }

}
