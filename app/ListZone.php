<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListZone extends Model
{
    protected $fillable = ['timezone_id', 'loc'];

    public function timezone() {

        return $this->belongsTo('App\ListTimezone', 'timezone_id', 'id');

    }

}
