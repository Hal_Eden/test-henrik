<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Timezone extends Model
{
    protected $table = 'timezone';

    public function zone()
    {
        return $this->belongsTo('App\Zone', 'zone_id', 'zone_id');
    }

}
