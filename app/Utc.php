<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class utc extends Model
{
    protected $fillable = ['timezone'];
}
