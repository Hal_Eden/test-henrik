
// Convert Time To Currently Selected Format
function formatTime(timeString, timeFormat) {

    switch(timeFormat) {
        case "0":
            timeString = moment(timeString, ["HH:mm", "hh:mm A"]);
            if(timeString.isValid()) {
                return timeString.format("hh:mm A");
            }
            return "";
        case "1":
            timeString = moment(timeString, ["HH:mm", "hh:mm A"]);
            if(timeString.isValid()) {
                return timeString.format("HH:mm");
            }
            return "";
    }

    return timeString;

}

// Get Time Of Selected Zone
function getTime(type, offset) {
    switch(type) {
        case 'zone':
            return moment().utcOffset(parseInt(offset));
        case 'loc':
            return moment().tz(offset);
    }
}

// Get Date Of Selected Zone
function getDate(type, offset) {
    switch(type) {
        case 'zone':
            return moment().utcOffset(parseInt(offset)).format("ddd, D MMM YYYY");
        case 'loc':
            return moment().tz(offset).format("ddd, D MMM YYYY");
    }
}

// Change Time Format 12/24
function formatDate(timeDate, timeFormat) {

    switch(timeFormat) {
        case "0":
            return timeDate.format("hh:mm A");
        case "1":
            return timeDate.format("HH:mm");
    }

    return "";

}

// Get Default Time For Comparing
function getDefaultDate(compareTimeText, loc) {

    let def = loc.filter(function (item) {
        return item.value == compareTimeText;
    })[0];

    if (def) {
        let type = def.type;
        let offset = def.offset;
        let zoneTime = getTime(type, offset);

        return zoneTime;
    }

    return "";

}

// Set New Time / Set Default Time
function setTimes(newTime, zoneNode, timeNode, pHolder, defaultTime) {

    if(newTime) {
        timeNode.val(newTime);
    } else {
        zoneNode.prop('placeholder', pHolder);
        zoneNode.val('');
        timeNode.val(defaultTime);
    }

}

// Generate List For Selected Time
function generateTimeList(time, item, timeFormat) {

    let generatedDate = getDate(item.type, item.offset);
    let generatedTime = getTime(item.type, item.offset);

    if(urlParams.ae && urlParams.aet) {

        let timeActiveEvent = getDefaultDate(urlParams.ae, loc_hints);
        let timeCompare = getDefaultDate(item.value, loc_hints);
        timeActiveEvent = moment(timeActiveEvent.format(fmt), fmt);
        timeCompare = moment(timeCompare.format(fmt), fmt);
        let timeDiffs = timeActiveEvent.diff(timeCompare);
        generatedTime = moment(urlParams.aet, 'x').subtract(timeDiffs);
        console.log(urlParams.aet);
        time = formatDate(generatedTime, timeFormat);
        generatedDate = generatedTime.format("ddd, D MMM YYYY");
        console.log(generatedTime);
        console.log(generatedDate);

    }

    let li = '<li class="times-li" data-name="' + item.value + '" data-id="' + eventIds + '" style="display: none" id="times-' + eventIds + '">' +
                '<div class="row">' +
                    '<div class="col-6">' +
                        '<img src="/img/flags/' + item.abbr.toString().toLowerCase() + '.png" alt="">' +
                        '<span>' + item.value + '</span>' +
                    '</div>' +
                    '<div class="col-3 datepick">' +
                        '<input class="times-input times-date datepicker" />' +
                    '</div>' +
                    '<div class="col-3">' +
                        '<input class="times-input times-time" value="' + time + '" />' +
                    '</div>' +
                '</div>' +
                '<span class="delete-time">&#10006;</span>' +
              '</li>';

    var params = [];
    var genTime = generatedTime;
    params.push(item.value);
    params.push(item.abbr);
    params.push(getMilliseconds(genTime));
    eventParams.events[eventIds] = params;

    $(li).appendTo('#times-ul').show('fast');

    var datePick = $("#times-" + eventIds).find(".datepicker");

    datePick.datepicker({
        onSelect: function(date) {
            let thisParent = $(this).closest('.times-li');
            let thisId = thisParent.data("id");
            let thisTime = thisParent.find(".times-time").val();
            let newTime = moment(date + " " + thisTime, ["ddd, DD MMM YYYY hh:mm A", "ddd, DD MMM YYYY HH:mm"]);
            let thisName = thisParent.data("name");
            eventParams.events[thisId][2] = getMilliseconds(newTime);

            let thisZoneTime = getDefaultDate(thisName, loc_hints);
            thisZoneTime = moment(thisZoneTime.format(fmt), fmt);
            let thisSiblings = thisParent.siblings();

            urlParams.ae = thisName;
            urlParams.aet = eventParams.events[thisId][2];
            console.log(urlParams.aet);

            thisSiblings.each(function() {
                let siblingZoneTime = getDefaultDate($(this).data("name"), loc_hints);
                siblingZoneTime = moment(siblingZoneTime.format(fmt), fmt);
                let timeDiff = thisZoneTime.diff(siblingZoneTime);
                let siblingNewTime = newTime.clone().subtract(timeDiff);
                let siblingDate = siblingNewTime.format("ddd, D MMM YYYY");
                $(this).find(".datepicker").datepicker("setDate", siblingDate);
                $(this).find(".times-time").val(formatDate(siblingNewTime, timeFormat));
                eventParams.events[$(this).data("id")][2] = getMilliseconds(siblingNewTime);
            });

            updateUrl();
        }
    });
    datePick.datepicker("option", "dateFormat", "D, d M yy");
    datePick.datepicker("setDate", generatedDate);
    eventIds++;

}

// Update URL After Data Changes
function updateUrl() {

    var url = '';
    for (var key in urlParams) {

        if (url != "") {
            url += "&";
        }

        url += key + "=" + encodeURIComponent(urlParams[key]);

    }

    url += "&" + $.param(eventParams);
    url = '?' + url;

    history.pushState(null, '', url);

}

// Update Time Differences Between Local And Compare Times
function updateDiffs(txt, data) {

    let locTime;
    let compTime = getDefaultDate(myDefs.compare, data);

    if(myDefs.local === txt) {
        locTime = moment();
    } else {
        locTime = getDefaultDate(myDefs.local, data);
    }

    locTime = moment(locTime.format(fmt), fmt);
    compTime = moment(compTime.format(fmt), fmt);

    diffs = locTime.diff(compTime);

}

// Change Time Relative To Diffs
function convertDiffs(thisTime, locOrComp, timeFormat) {

    let newTime = thisTime;

    switch(locOrComp) {
        case "local":
            newTime = thisTime.clone().subtract(diffs);
            $('#zone-compare-time').val(formatDate(newTime, timeFormat));
            break;
        case "compare":
            newTime = thisTime.clone().add(diffs);
            $('#zone-local-time').val(formatDate(newTime, timeFormat));
            break;
    }

}

// Copy Current URL On Button Click
function copyUrl() {

    var dummy = document.createElement('input'),
        text = window.location.href;

    document.body.appendChild(dummy);
    dummy.value = text;
    dummy.select();
    document.execCommand('copy');
    document.body.removeChild(dummy);

    $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
        $("#success-alert").slideUp(500);
    });

}

// Get Moment Date As Milliseconds
function getMilliseconds(time) {

    return moment(time.format(fmt), fmt).format('x');

}
