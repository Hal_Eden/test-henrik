var urlParams = {};
var eventParams = { events: [] };
var eventIds = 0;
var fmt = "llll";
var myDefs = {};
var diffs = 0;

$(document).ready(function() {

    // Local Time Data
    urlParams.lz = formParams["lz"] ? formParams["lz"] : "";
    urlParams.lt = formParams["lt"] ? formParams["lt"] : "";

    // Compare Time Data
    urlParams.cz = formParams["cz"] ? formParams["cz"] : "";
    urlParams.ct = formParams["ct"] ? formParams["ct"] : "";

    // Set Active Event;
    urlParams.ae = formParams["ae"] ? formParams["ae"] : "";
    urlParams.aet = formParams["aet"] ? formParams["aet"] : "";

    // Node Variables
    var timeFormatElem = $("input[name='time_format']");
    var timeOrEvent = $("input[name='timeorevent']");
    var localZone = $('#zone-local');
    var localTime = $('#zone-local-time');
    var compareZone = $('#zone-compare');
    var compareTime = $('#zone-compare-time');
    var autocompletes = $('#zone-local, #zone-compare, #zone-single');
    var timeConverter = $('#time-converter');
    var eventPlanner = $('#event-planner');
    var timesUl = $('#times-ul');

    // Date And Time Variables
    var timeOrEventVal = $("input[name='timeorevent']:checked").val();
    var timeFormat = $("input[name='time_format']:checked").val();
    var localTimeText = urlParams.lz ? urlParams.lz : "Your Local Time";
    var compareTimeText = urlParams.cz ? urlParams.cz : "United States, San Francisco";
    var locTime = urlParams.lt ? moment(urlParams.lt, 'x') : moment();
    var defLocalTime = formatDate(locTime, timeFormat);
    var defCompareTime = urlParams.ct ? formatDate(moment(urlParams.ct, 'x'), timeFormat) : formatDate(getDefaultDate(compareTimeText, loc_hints), timeFormat);

    // Define Default Local And Compare Times
    myDefs.local = urlParams.lz ? urlParams.lz : localTimeText;
    myDefs.compare = urlParams.cz ? urlParams.cz : compareTimeText;

    // Initial Actions
    $("#success-alert").hide();
    updateDiffs(localTimeText, loc_hints);

    // Form Type / Time Format
    urlParams.t = timeOrEventVal;
    urlParams.tf = timeFormat;

    // Create Jquery Autocomplete
    autocompletes.autocomplete({
        source: function(request, response) {
            let results = $.ui.autocomplete.filter(loc_hints, request.term);
            response(results.slice(0, 10));
        },
        minLength: 1,
        select: function(event, ui) {
            var timeId = '#' + $(this).prop('id') + '-time';
            var type = ui.item.type;
            var offset = ui.item.offset;
            var zoneTime = getTime(type, offset);
            var zoneTimeFormat = formatDate(zoneTime, timeFormat);
            var zoneType = $(this).data("timeorevent");
            var formType = $(this).data("formtype");

            switch(zoneType) {
                case "time":
                    myDefs[formType] = ui.item.value;
                    urlParams[formType.charAt(0) + "z"] = ui.item.value;
                    urlParams[formType.charAt(0) + "t"] = getMilliseconds(zoneTime);
                    $(timeId).val(zoneTimeFormat);

                    updateDiffs(localTimeText, loc_hints);
                    convertDiffs(zoneTime, formType, timeFormat);

                    break;
                case "event":
                    generateTimeList(zoneTimeFormat, ui.item, timeFormat);
                    break;
            }

            if(zoneType === "time") {
                $(timeId).val(zoneTimeFormat);
            }

            $(this).prop("placeholder", ui.item.value);

            updateUrl();
        },
        html: true, 
        open: function(event, ui) {
            $(".ui-autocomplete").css("z-index", 1000);
        },
        change: function(event, ui) {
            $(this).val("");
        },
        close: function(event, ui) {
            $(this).val("");
        }
    }).data("ui-autocomplete")._resizeMenu = function () {
        var ul = this.menu.element;
        ul.outerWidth(this.element.outerWidth());
    };

    // Customize Autocomplete View
    autocompletes.each(function() {
        $(this).data("ui-autocomplete")._renderItem = function(ul, item) {
            return $("<li class='au-list'><div><span>" + item.value + "</span><img src='/img/flags/" + item.abbr.toString().toLowerCase() + ".png'></div></li>").appendTo(ul);
        };
        $(this).data("ui-autocomplete")._resizeMenu = function () {
            var ul = this.menu.element;
            ul.outerWidth(this.element.outerWidth());
        };
    });

    setTimes('', localZone, localTime, localTimeText, defLocalTime);
    setTimes('', compareZone, compareTime, compareTimeText, defCompareTime);

    // Delete Times List Items On Click
    $(document).on("click", ".delete-time", function() {

        let parEl = $(this).parent();
        eventParams.events[parEl.data("id")] = null;
        // console.log(eventParams.events);
        $(this).parent().remove();

        updateUrl();

    });

    // Switch Date and Time Converter Forms
    timeOrEvent.on('change', function() {
        timeConverter.toggle();
        eventPlanner.toggle();
        timesUl.toggle();
        urlParams.t = $(this).val();
        updateUrl();
    });

    // Check Date Validity When Changing Dates
    localTime.on('blur', function() {

        let thisTime = $(this).val();
        thisTime = moment(thisTime, ["hh:mm A", "HH:mm"]);
        let newTime = thisTime.clone().subtract(diffs);
        urlParams.lt = getMilliseconds(thisTime);

        switch(timeFormat) {
            case "0":
                thisTime = thisTime.format("hh:mm A");
                compareTime.val(newTime.format("hh:mm A"));
                break;
            case "1":
                thisTime = thisTime.format("HH:mm");
                compareTime.val(newTime.format("HH:mm"));
                break;
        }

        urlParams.ct = getMilliseconds(newTime);

        setTimes(thisTime, localZone, localTime, localTimeText, defLocalTime);

        updateUrl();

    });

    compareTime.on('blur', function() {

        let thisTime = $(this).val();
        thisTime = moment(thisTime, ["hh:mm A", "HH:mm"]);
        let newLocalTime = thisTime.clone().add(diffs);
        urlParams.ct = getMilliseconds(thisTime);

        switch(timeFormat) {
            case "0":
                thisTime = thisTime.format("hh:mm A");
                localTime.val(newLocalTime.format("hh:mm A"));
                break;
            case "1":
                thisTime = thisTime.format("HH:mm");
                localTime.val(newLocalTime.format("HH:mm"));
                break;
        }

        urlParams.lt = getMilliseconds(newLocalTime);

        setTimes(thisTime, compareZone, compareTime, compareTimeText, defCompareTime);

        updateUrl();

    });

    $(document).on("blur", ".times-time", function() {

        let thisTime = $(this).val();
        let thisParent = $(this).closest(".times-li");
        let thisDateNode = thisParent.find('.datepicker');
        let thisId = thisParent.data("id");
        let thisDate = thisDateNode.datepicker("getDate");
        let thisSiblings = thisParent.siblings();
        let thisName = thisParent.data("name");
        let thisZoneTime = getDefaultDate(thisName, loc_hints);
        thisZoneTime = moment(thisZoneTime.format(fmt), fmt);
        thisDate = moment(Date.parse(thisDate)).format("ddd, DD MMM YYYY");
        thisTime = moment(thisTime, ["hh:mm A", "HH:mm"]);

        switch(timeFormat) {
            case "0":
                thisTime = thisTime.format("hh:mm A");
                break;
            case "1":
                thisTime = thisTime.format("HH:mm");
                break;
        }

        $(this).val(thisTime);
        let newTime = moment(thisDate + " " + thisTime, ["ddd, DD MMM YYYY hh:mm A", "ddd, DD MMM YYYY HH:mm"]);

        thisSiblings.each(function() {
            let siblingZoneTime = getDefaultDate($(this).data("name"), loc_hints);
            siblingZoneTime = moment(siblingZoneTime.format(fmt), fmt);
            let timeDiff = thisZoneTime.diff(siblingZoneTime);
            let siblingNewTime = newTime.clone().subtract(timeDiff);
            let siblingDate = siblingNewTime.format("ddd, D MMM YYYY");
            $(this).find(".datepicker").datepicker("setDate", siblingDate);
            $(this).find(".times-time").val(formatDate(siblingNewTime, timeFormat));
            eventParams.events[$(this).data("id")][2] = getMilliseconds(siblingNewTime);
        });

        eventParams.events[thisId][2] = getMilliseconds(newTime);
        urlParams.ae = thisName;
        urlParams.aet = eventParams.events[thisId][2];

        updateUrl();

    });

    // Generate Default Event Times List
    if(formParams.events) {
        var events = formParams.events.filter(function (el) {
            return el != null;
        });

        var eventsLength = events.length;

        for (var j = 0; j < eventsLength; j++) {

            var convertedTime = moment(events[j][2], 'x');
            var savedTime = "";
            var savedDate = convertedTime.format("ddd, D MMM YYYY");

            switch(timeFormat) {
                case "0":
                    savedTime = convertedTime.format("hh:mm A");
                    break;
                case "1":
                    savedTime = convertedTime.format("HH:mm");
                    break;
            }

            let li = '<li class="times-li" data-name="' + events[j][0] + '" data-id="' + eventIds + '" id="times-' + eventIds + '">' +
                        '<div class="row">' +
                            '<div class="col-6">' +
                                '<img src="/img/flags/' + events[j][1].toString().toLowerCase() + '.png" alt="">' +
                                '<span>' + events[j][0] + '</span>' +
                            '</div>' +
                            '<div class="col-3 datepick">' +
                                '<input type="text" size="30" class="times-input times-date datepicker" />' +
                            '</div>' +
                            '<div class="col-3 timepick">' +
                                '<input class="times-input times-time" value="' + savedTime + '" />' +
                            '</div>' +
                        '</div>' +
                        '<span class="delete-time">&#10006;</span>' +
                      '</li>';

            var params = [];
            params.push(events[j][0]);
            params.push(events[j][1]);
            params.push(events[j][2]);
            eventParams.events[eventIds] = params;

            $(li).appendTo('#times-ul');

            var datePick = $("#times-" + eventIds).find(".datepicker");

            datePick.datepicker({
                onSelect: function(date) {
                    let thisParent = $(this).closest('.times-li');
                    let thisId = thisParent.data("id");
                    let thisTime = thisParent.find(".times-time").val();
                    let newTime = moment(date + " " + thisTime, ["ddd, DD MMM YYYY hh:mm A", "ddd, DD MMM YYYY HH:mm"]);
                    let thisName = thisParent.data("name");
                    eventParams.events[thisId][2] = getMilliseconds(newTime);
                    let thisZoneTime = getDefaultDate(thisName, loc_hints);

                    thisZoneTime = moment(thisZoneTime.format(fmt), fmt);
                    let thisSiblings = thisParent.siblings();

                    urlParams.ae = thisName;
                    urlParams.aet = eventParams.events[thisId][2];
                    console.log(urlParams.aet);

                    thisSiblings.each(function() {
                        let siblingZoneTime = getDefaultDate($(this).data("name"), loc_hints);
                        siblingZoneTime = moment(siblingZoneTime.format(fmt), fmt);
                        let timeDiff = thisZoneTime.diff(siblingZoneTime);
                        let siblingNewTime = newTime.clone().subtract(timeDiff);
                        let siblingDate = siblingNewTime.format("ddd, D MMM YYYY");
                        $(this).find(".datepicker").datepicker("setDate", siblingDate);
                        $(this).find(".times-time").val(formatDate(siblingNewTime, timeFormat));
                        eventParams.events[$(this).data("id")][2] = getMilliseconds(siblingNewTime);
                    });

                    updateUrl();
                }
            });
            datePick.datepicker("option", "dateFormat", "D, d M yy");
            datePick.datepicker("setDate", savedDate);

            eventIds++;
        }

    }

    // Set Default Form States
    if(timeOrEventVal == "1") {
        timeConverter.hide();
        eventPlanner.show();
        timesUl.show();
    }

    // Change Time Formats
    timeFormatElem.on('change', function() {

        timeFormat = $(this).val();
        urlParams.tf = timeFormat;

        $('.times-time').each(function() {
            var thisTime = $(this).val();

            switch(timeFormat) {
                case "0":
                    thisTime = moment(thisTime, ["hh:mm A", "HH:mm"]).format("hh:mm A");
                    break;
                case "1":
                    thisTime = moment(thisTime, ["hh:mm A", "HH:mm"]).format("HH:mm");
                    break;
            }

            $(this).val(thisTime);
        });

        let localTimeNew = formatTime(localTime.val(), timeFormat);
        let compareTimeNew = formatTime(compareTime.val(), timeFormat);

        setTimes(localTimeNew, localZone, localTime, timeFormat, localTimeText, defLocalTime);
        setTimes(compareTimeNew, compareZone, compareTime, timeFormat, compareTimeText, defCompareTime);

        updateUrl();

    });

});