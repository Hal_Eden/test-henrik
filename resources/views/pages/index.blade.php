@extends('layouts.master')

@section('mainContent')

    <div class="container">
        <div class="row">
            <div class="col-8 mx-auto">
                <div class="row">
                    <div class="btn-group btn-group-toggle col-5 mx-auto mb-3" id="time_event" data-toggle="buttons">
                        <label class="btn btn-secondary custom-button {{ $form_params["t"] !== "1" ? "active" : "" }}">
                            <input type="radio" name="timeorevent" value="0" id="time-converter-radio" autocomplete="off" {{ $form_params["t"] !== "1" ? "checked" : "" }}> Time Converter
                        </label>
                        <label class="btn btn-secondary custom-button {{ $form_params["t"] === "1" ? "active" : "" }}">
                            <input type="radio" name="timeorevent" value="1" id="event-planner-radio" autocomplete="off" {{ $form_params["t"] === "1" ? "checked" : "" }}> Event Planner
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="btn-group btn-group-toggle mx-auto mb-3" data-toggle="buttons">
                        <label class="btn btn-secondary {{ $form_params["tf"] !== "1" ? "active" : "" }}">
                            <input type="radio" name="time_format" value="0" id="time-format-12" autocomplete="off" {{ $form_params["tf"] !== "1" ? "checked" : "" }}> 12
                        </label>
                        <label class="btn btn-secondary {{ $form_params["tf"] === "1" ? "active" : "" }}">
                            <input type="radio" name="time_format" value="1" id="time-format-24" autocomplete="off" {{ $form_params["tf"] === "1" ? "checked" : "" }}> 24
                        </label>
                    </div>
                </div>
                <div class="row">
                    <form class="time-container col-12" id="time-converter">
                        <div class="form-group row">
                            <div class="col-8 mx-auto">
                                <div class="row">
                                    <div class="col-md-6 border-right autocomplete">
                                        <input data-timeorevent="time" data-formtype="local" id="zone-local" type="text" class="form-control" name="zone_local" placeholder="">
                                    </div>
                                    <div class="col-md-6 border-left autocomplete">
                                        <input data-timeorevent="time" data-formtype="compare" id="zone-compare" type="text" class="form-control" name="zone_compare" placeholder="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-5 mx-auto">
                                <div class="row">
                                    <div class="col-md-6 border-right">
                                        <input id="zone-local-time" data-formtype="local" type="text" class="form-control" name="time_local">
                                    </div>
                                    <div class="col-md-6 border-left">
                                        <input id="zone-compare-time" data-formtype="compare" type="text" class="form-control" name="time_compare">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="row">
                    <form class="time-container col-12" id="event-planner">
                        <div class="form-group row">
                            <div class="col-6 mx-auto">
                                <input data-timeorevent="event" data-formtype="event" id="zone-single" type="text" class="form-control" name="zone" placeholder="Type a city or a country name...">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="form-group row">
                    <button type="button" class="btn btn-secondary mx-auto" onclick="copyUrl();">Copy URL</button>
                </div>
                <div class="alert alert-success" id="success-alert">
                    URL Copied To Clipboard
                </div>
            </div>
            <ul id="times-ul"></ul>
        </div>
    </div>

    <script type="text/javascript" defer>
        var loc_hints = Object.values(@json($locations));
        var formParams = @json($form_params);
        // loc_hints = loc_hints.forEach(function(part, index, theArray) {
        //     part.value = decodeURIComponent(escape(part.value));
        //     theArray[index] = part;
        // });
        // console.log(formParams);
    </script>

@endsection
