<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Date and Time converting form page
Route::get('/', 'MainController@dateTime')->name('date.time');

//Ajax Search Time Zones
Route::post('/search', 'MainController@search')->name('ajax.search');
Route::post('/search/data', 'MainController@searchData')->name('ajax.search.data');
